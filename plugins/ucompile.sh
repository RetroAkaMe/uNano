#!/bin/bash
function loop () {
  clear
  bash compiler.sh
  echo type reload to reload the compiler
  echo type exit to exit the application
  read action
  if [[ $action == "reload" ]];
  then
    loop
  elif [[ $action == "exit" ]];
  then
    exit
  fi
}

function load () {
  clear
  echo path to your java directory
  echo put the path in quotes
  echo ---------------
  read sessionf
  cd $sessionf
  bash compiler.sh
  echo commands
  echo type reload to reload a compiler
  read action
  if [[ $action == "reload" ]];
  then
    loop
  fi
}
clear
echo welcome to ucompile v1.0
echo please note that qcompile currently only compiles java
echo type n to create a new compiler
echo type l to load a compiler
echo type x to exit
read actionmain
if [[ $actionmain == "n" ]];
then
  echo uCompile wizard
  echo ---------------
  echo path to your java directory
  echo do not include the java file
  echo put the path in quotes
  read D
  cd $D
  echo ---------------
  echo what is the name of your java file? if you wanna       echo include all java files in this
  echo directory, type *.java
  read F
  echo ---------------
  echo give us the filename of your manifest.mf MUST BE IN    echo THE SAME DIRECTORY THAN THE JAVA FILE!
  read M
  echo ---------------
  echo how should your .jar be called? add .jar to the end
  read J
  clear
  echo uCompile Wizard
  echo ---------------
  printf "javac $F \njar -cvmf $M $J *.class\nbash $J\nexit" > compiler.sh
  chmod +x compiler.sh
  echo adding run perms
  echo successfully created compiler!
  echo you can now leave the wizard
  bash compiler.sh
elif [[ $actionmain == "l" ]];
then
  load
elif [[ $actionmain == "x" ]];
then
  exit
fi